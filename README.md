# 生成数据库设计文档

#### 介绍

通过读取数据库的表和表内的字段，生成word文档，可提高文档书写效率<br>
项目基于博客[mysql数据库自动生成数据库开发设计文档](https://blog.csdn.net/lq18050010830/article/details/78851180)<br>
和github项目[lv617DbTest](https://github.com/BeliveYourSelf/lv617DbTest)<br>
项目演示地址：[vue版本](https://www.wenruitao.top/pddvue/)或[静态页面版本](https://www.wenruitao.top/pdd/)<br>
请注意，线上版本仅支持在线数据库，且在线数据库需要允许外网访问，否则数据库不会切换<br>
数据库成功切换后，10分钟内可进行下载操作，10分钟后移除数据源，自动切换回演示数据库

#### 软件架构

核心框架：springboot 2.5.2

数据层: mybatis

项目构建工具：maven

加密工具: jasypt

Web容器：tomcat

前端1开源工具包：bootstrap
前端1数据交互：ajax

前端2框架：vue3.0 + element-plus1.0.2
前端2数据交互：axios


#### 安装教程

1. 项目可通过maven打包成一个war，放入tomcat运行
1. 若导入开发工具eclipse或idea，可直接运行src/main/java/com/wrt/produce/ProduceDataBaseDocument.java启动项目
1. 打包或运行前，请注意修改application.yml中的数据库配置
1. 由于引入了加密工具，请先通过src/main/java/com/wrt/produce/util/TestEncryptor.java生成加密后的用户名和密码<br>
如果你的本地数据库和我一样都是用'root'作为用户名和密码，那么只需要修改数据库名称就行，不需要修改配置中的用户名和密码


#### 使用说明

1. 项目默认使用8085端口
1. 项目启动后，可通过浏览器访问首页http://localhost:8085/index.html<br>
如下图所示：
![默认首页图](https://images.gitee.com/uploads/images/2021/0713/212853_eebaa6e7_4897429.png "屏幕截图 2021-07-13 212831.png")
1. 页面上会显示当前连接的数据库名称，可点击旁边的“立即下载”按钮进行文档下载
（p.s.如果只是自己用，完全可以参考介绍中的博客自行做一个小工具，没必要以项目的形式来实现）
1. 如果需要对其他数据库进行文档生成，可以在表单中填入数据库地址、用户名和密码
1. 系统将会修改配置文件并自动重启，重启完成后可下载文档
1. 还可以使用src/main/front_vue路径下的vue版本前端，通过vue-cli编译并运行后，即可使用<br>
页面样式和静态html页面相似

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
