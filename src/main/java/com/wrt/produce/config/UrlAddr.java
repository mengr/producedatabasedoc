package com.wrt.produce.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

/**
 * 域名地址配置
 * 
 * @author 文瑞涛
 */
@ConfigurationProperties(prefix = "urladdr")
@Getter
@Setter
public class UrlAddr {

	/**
	 * 域名地址
	 */
	private String web;
	/**
	 * 跨域地址
	 */
	private String crossOrigins;

	public String[] getAllOrigins() {
		return (this.web + "," + this.crossOrigins).split(",");
	}
}
