package com.wrt.produce.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 
 * 网络配置
 * @author 文瑞涛
 */
@Configuration
@EnableConfigurationProperties({ UrlAddr.class })
public class WebConfig implements WebMvcConfigurer {
	@Autowired
	private UrlAddr url;

	/**
	 * 跨域设置
	 */
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins(url.getAllOrigins()).allowCredentials(true).allowedMethods("*")
				.allowedHeaders("*").exposedHeaders("Set-Cookie").maxAge(3600L);
	}
}
