package com.wrt.produce.config;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;
import com.wrt.produce.source.DynamicDataSource;

/**
 * 数据源配置
 * 
 * @author 文瑞涛
 * @date 2021年9月6日 下午10:14:13
 */
@Configuration
public class DataSourceConfig {
	
	/**
	 * 默认数据源Bean
	 * 
	 * @return 数据源对象
	 */
	@Bean(initMethod = "init", destroyMethod="close")
    @ConfigurationProperties("spring.datasource.druid")
    public DataSource defaultDataSource() {
		System.setProperty("druid.mysql.usePingMethod", "false");
        return DruidDataSourceBuilder.create().build();
    }

	/**
	 * 动态数据源Bean
	 * 
	 * @param defaultDataSource 默认数据源Bean
	 * 
	 * @return 动态数据源Bean
	 */
    @Bean
    @Primary
    @DependsOn({"defaultDataSource"})
    public DynamicDataSource dataSource(DataSource defaultDataSource) {
        return new DynamicDataSource(defaultDataSource);
    }
}
