package com.wrt.produce.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wrt.produce.service.DataSourceChangeService;

/**
 * 数据源更改接口
 * 
 * @author 文瑞涛
 * @date 2021年9月6日 下午10:22:39
 */
@RestController
public class DataSourceChangeController {

	@Resource
	DataSourceChangeService dataSourceChangeService;

	/**
	 * 更改数据源
	 * 
	 * @param url 数据库地址
	 * @param username 用户名
	 * @param password 密码
	 * @param request http请求
	 */
	@PostMapping("/change")
	public void change(String url, String username, String password, HttpServletRequest request) {
		if (StringUtils.isBlank(url) || StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
			return;
		}
		boolean testresult = dataSourceChangeService.testConnect(url, username, password);
		if (testresult) {
			dataSourceChangeService.changeDataSorce(url, username, password, request.getSession().getId());
		}
	}

}
