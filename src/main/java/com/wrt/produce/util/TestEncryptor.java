package com.wrt.produce.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.jasypt.encryption.StringEncryptor;
import org.jasypt.salt.RandomSaltGenerator;
import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEByteEncryptor;
import com.ulisesbocchio.jasyptspringboot.encryptor.SimplePBEStringEncryptor;

public class TestEncryptor {
	
	public static void main(String[] args) {
		SimplePBEByteEncryptor sp = new SimplePBEByteEncryptor();
		sp.setPassword("abc");
		sp.setAlgorithm("PBEWithMD5AndDES");
		sp.setSaltGenerator(new RandomSaltGenerator());
		sp.setIterations(1000);
		StringEncryptor encryptor = new SimplePBEStringEncryptor(sp);
		
		//加密
        String name = encryptor.encrypt("root");
        String password = encryptor.encrypt("root");
        System.out.println("name 密文: " + name);
        System.out.println("password 密文: " + password);
        
      //解密
        String decrypt1 = encryptor.decrypt(name);
        String decrypt2 = encryptor.decrypt(password);
        System.out.println(decrypt1 + "------------" + decrypt2);
        
        System.out.println(RandomStringUtils.randomAlphanumeric(7));
        
        
	}
}
