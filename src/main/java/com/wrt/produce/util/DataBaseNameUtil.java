package com.wrt.produce.util;

import org.apache.commons.lang3.StringUtils;

public class DataBaseNameUtil {

	private DataBaseNameUtil() {

	}

	/**
	 * 通过地址获取数据库名称
	 * 
	 * @param url 数据库地址
	 * @return 数据库名称
	 */
	public static String getDataBaseNameByUrl(String url) {
		if (StringUtils.isNotBlank(url)) {
			return url.substring(url.indexOf("/", url.lastIndexOf(":")) + 1, url.indexOf("?"));
		} else {
			return StringUtils.EMPTY;
		}
	}
}
