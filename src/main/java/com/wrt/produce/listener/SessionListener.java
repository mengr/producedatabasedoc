package com.wrt.produce.listener;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.wrt.produce.source.DynamicDataSource;

import lombok.extern.log4j.Log4j2;

/**
 * session监听器
 * 
 * @author 文瑞涛
 * @date 2021年9月6日 下午10:24:20
 */
@WebListener
@Log4j2
public class SessionListener implements HttpSessionListener {

	/**
	 * session销毁时恢复初始数据源
	 * 并移除可能存在的更变的数据源
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent se) {
		DynamicDataSource dataSource = (DynamicDataSource) getObjectFromApplication(se.getSession().getServletContext(),
				"dataSource");
		if (null != dataSource) {
			dataSource.clearDataSourceKey();
			dataSource.removeDataSource(se.getSession().getId());
		} else {
			log.warn("未移除数据源");
		}
	}

	/**
	 * 通过WebApplicationContextUtils 得到Spring容器的实例。根据bean的名称返回bean的实例。
	 * 
	 * @param servletContext :ServletContext上下文
	 * @param beanName       :要取得的Spring容器中Bean的名称
	 * @return 返回Bean的实例
	 */
	private Object getObjectFromApplication(ServletContext servletContext, String beanName) {
		// 通过WebApplicationContextUtils 得到Spring容器的实例。
		ApplicationContext application = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		// 返回Bean的实例。
		return null == application ? null : application.getBean(beanName);
	}
}
