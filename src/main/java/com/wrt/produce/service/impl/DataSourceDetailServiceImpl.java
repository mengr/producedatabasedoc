package com.wrt.produce.service.impl;

import com.lowagie.text.*;
import com.lowagie.text.Font;
import com.lowagie.text.rtf.RtfWriter2;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import com.wrt.produce.dao.DataSourceMapper;
import com.wrt.produce.entity.TableDetail;
import com.wrt.produce.entity.Tables;
import com.wrt.produce.service.DataSourceDetailService;

import lombok.extern.log4j.Log4j2;

/**
 * 数据库文档服务实现类
 * 
 * @author 文瑞涛
 * @date 2021年7月13日 上午11:13:24
 */
@Service
@Log4j2
public class DataSourceDetailServiceImpl implements DataSourceDetailService {

	private static final String DEFAULT_TITLE = "数据库表设计文档";

	@Autowired
	private DataSourceMapper dataSourceMapper;

	@Override
	public List<TableDetail> getDataSourceDetail(String dbName, String tableName) {
		return dataSourceMapper.getDataDetail(dbName, tableName);
	}

	@Override
	public List<Tables> getAllDataSourceName(String tableName) {
		return dataSourceMapper.getAllDataSourceName(tableName);
	}

	@Override
	public File toWord(String dbName, List<Tables> listAll, Boolean setTableHeadCenter) {
		// 创建word文档,并设置纸张的大小
		Document document = new Document(PageSize.A4);
		String path = ClassUtils.getDefaultClassLoader().getResource("").getPath();
		File pathFile = new File(path, "temp");
		if (!pathFile.exists()) {
			pathFile.mkdirs();
		}
		File docFile = new File(pathFile.getAbsolutePath(), dbName + DEFAULT_TITLE + ".doc");
		try {
			// 创建word文件
			RtfWriter2.getInstance(document, new FileOutputStream(docFile));
			document.open();
			// 设置文档标题
			Paragraph p = new Paragraph(DEFAULT_TITLE, new Font(Font.NORMAL, 24, Font.BOLD, new Color(0, 0, 0)));
			p.setAlignment(1);
			document.add(p);
			/* * 创建表格 通过查询出来的表遍历 */
			int i = 0;
			for (Tables datatable : listAll) {
				// 表名
				String tableName = datatable.getTableName();
				// 表说明
				String tableComment = datatable.getTableComment();
				// 获取某张表的所有字段说明
				List<TableDetail> list = this.getDataSourceDetail(dbName, tableName);
				// 构建表说明
				String all = (i + 1) + " 表名：" + tableName + " [" + tableComment + "]";
				i++;
				// 创建有6列的表格
				Table table = new Table(6);
				document.add(new Paragraph(StringUtils.EMPTY));
				table.setBorderWidth(1);
				table.setPadding(0);
				table.setSpacing(0);
				/*
				 * 添加表头的元素，并设置表头背景的颜色
				 */
				Color chade = new Color(176, 196, 222);
				table.addCell(createCell("序号", chade, setTableHeadCenter));
				table.addCell(createCell("字段名", chade, setTableHeadCenter));
				table.addCell(createCell("类型", chade, setTableHeadCenter));
				table.addCell(createCell("是否为空", chade, setTableHeadCenter));
				table.addCell(createCell("主键", chade, setTableHeadCenter));
				table.addCell(createCell("字段说明", chade, setTableHeadCenter));
				table.endHeaders();// 表头结束
				// 表格的主体，
				int k = 0;
				for (TableDetail td : list) {
					// 获取某表每个字段的详细说明
					table.addCell(String.valueOf(k + 1));
					table.addCell(td.getField());
					table.addCell(td.getType());
					table.addCell(td.getNull());
					table.addCell(td.getKey());
					table.addCell(td.getComment());
					k++;
				}
				Paragraph pheae = new Paragraph(all);
				// 写入表说明
				document.add(pheae);
				// 生成表格
				document.add(table);
			}
			return docFile;
		} catch (IOException | DocumentException e) {
			log.error(e.getMessage(), e);
		} finally {
			document.close();
		}
		return null;
	}
	
	/**
	 * 生成表头（标题行）单元格
	 * 
	 * @param name 单元格内容
	 * @param color 单元格背景色
	 * @param setCentern 是否居中
	 * 
	 * @return 单元格
	 */
	private Cell createCell(String name, Color color, Boolean setCenter){
		Cell cell = new Cell(name);// 单元格
		cell.setBackgroundColor(color);
		cell.setHeader(true);
		if (setCenter.booleanValue()) {
			cell.setVerticalAlignment(Element.ALIGN_CENTER);
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		}
		return cell;
	}
}
