package com.wrt.produce.service;

public interface DataSourceChangeService {
	
	/**
	 * 测试数据库是否能正常连接
	 * 
	 * @param url 数据库地址
	 * @param name 数据库用户名
	 * @param password 数据库密码
	 * 
	 * @return 是否能够连接数据库
	 */
	boolean testConnect(String url, String name, String password);

	/**
	 * 修改数据源
	 * 
	 * @param url 数据库地址
	 * @param name 数据库用户名
	 * @param password 数据库密码
	 * @param key 数据源识别标志
	 */
	void changeDataSorce(String url, String name, String password, String key);
	
}
