package com.wrt.produce.service;

import java.io.File;
import java.util.List;

import com.wrt.produce.entity.TableDetail;
import com.wrt.produce.entity.Tables;

/**
 * 数据库文档生成服务类
 * 
 * @author 文瑞涛
 * @date 2021年7月13日 上午11:13:24
 */
public interface DataSourceDetailService {
	
    /**
     * 根据表名称获取表的详细信息
     * 
     * @param dbName 数据库名称
     * @param tableName 表名称
     *
     * @return 所查询表字段详情集合
     */
    List<TableDetail> getDataSourceDetail(String dbName, String tableName);
    
    /**
     * 根据数据库名称获取数据库中表的名称和注释
     *
     * @param dbName 数据库名称
     * 
     * @return 所查询数据库表格集合
     */
    List<Tables> getAllDataSourceName(String dbName);
    
    /**
     * 将数据写入指定的word文档中
     *
     * @param dbName 数据库名称
     * @param listAll 数据库中所有的表格
     * @param isTableHeadCenter 是否将表头设置为居中显示
     * 
     * @return 生成的word文档
     */
    File toWord(String dbName, List<Tables> listAll, Boolean isTableHeadCenter);
}
