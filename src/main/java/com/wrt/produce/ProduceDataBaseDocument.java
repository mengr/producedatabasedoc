package com.wrt.produce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
public class ProduceDataBaseDocument {

	public static void main(String[] args) {
		SpringApplication.run(ProduceDataBaseDocument.class, args);
	}
	
}
