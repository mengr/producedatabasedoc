package com.wrt.produce.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.wrt.produce.entity.TableDetail;
import com.wrt.produce.entity.Tables;

import java.util.List;

@Mapper
public interface DataSourceMapper {
    /**
     * 根据表名称获取表的详细信息
     *
     *@author lv617
     *@Date 2018/9/4 16:55
     */
    @Select("SHOW FULL FIELDS FROM ${dbName}.${tableName}")
    List<TableDetail> getDataDetail(@Param("dbName") String dbName, @Param("tableName") String tableName);

    /**
     * 根据数据库名称获取数据库中表的名称和注释
     *
     *@author lv617
     *@Date 2018/9/4 16:55
     */
    @Select("select table_name as tableName, table_comment as tableComment from information_schema.tables where table_schema = #{dbName}")
    List<Tables> getAllDataSourceName(@Param("dbName") String dbName);
}
