package com.wrt.produce.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
/**
 * 数据库表
 * 
 * @author 文瑞涛
 * @date 2021年7月13日 下午12:00:51
 */
public class Tables {

	/**
	 * 表名称
	 */
	private String tableName;
	
	/**
	 * 表注释
	 */
	private String tableComment;
}
