package com.wrt.produce.entity;

import org.apache.commons.lang3.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
/**
 * 数据库表字段详情
 * 
 * @author 文瑞涛
 * @date 2021年7月13日 下午12:00:32
 */
public class TableDetail {

	/**
	 * 字段名
	 */
	private String Field;
	
	/**
	 * 类型
	 */
	private String Type;
	
	/**
	 * 是否为空
	 */
	private String Null;
	
	/**
	 * 主键
	 */
	private String Key;
	
	/**
	 * 注释
	 */
	private String Comment;
	
	/**
	 * 排序规则
	 */
	private String Collation;
	
	/**
	 * 默认值
	 */
	private String Default;
	
	/**
	 * 额外字段
	 */
	private String Extra;
	
	/**
	 * 权限
	 */
	private String Privileges;
	
	public String getNull() {
		return this.Null.equalsIgnoreCase("NO") ? "否" : "是";
	}
	
	public String getKey() {
		if (StringUtils.isNotBlank(this.Key)) {
			switch (this.Key) {
			case "PRI":
				return "主键约束";
			case "UNI":
				return "唯一约束";
			case "MUL":
				return "可以重复";
			default:
				return StringUtils.EMPTY;
			}
		} else {
			return StringUtils.EMPTY;
		}
	}
}
