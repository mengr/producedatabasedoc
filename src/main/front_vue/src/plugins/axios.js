import axios from 'axios'
import {
	ElMessage
} from 'element-plus'

axios.defaults.baseURL = ""
// 携带 cookie
axios.defaults.withCredentials = true
// 请求头，headers 信息
axios.defaults.headers['X-Requested-With'] = 'XMLHttpRequest'
// 默认 post 请求
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.defaults.headers['Cache-Control'] = 'no-cache'
//axios请求拦截器
axios.interceptors.request.use(config => {
	if (/get/i.test(config.method)) { //判断get请求
		config.params = config.params || {};
		config.params.t = Date.parse(new Date()) / 1000; //添加时间戳
	}
	return config;
}, error => {
	return Promise.reject(error);
})
// 请求拦截器，内部根据返回值，重新组装，统一管理。
axios.interceptors.response.use(res => {
	if (null == res.data) {
		ElMessage.error('服务端异常！')
		return Promise.reject(res)
	}
	if (res.data.resultCode != 200) {
		if (res.data.message) {
			ElMessage.error(res.data.message)
		}
	}

	return res;
})

export default axios
